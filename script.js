/* AJAX це асинхронний JS. Він доступний до використання через API браузера і корисний тим, 
 що завдякий йому ми можемо виконувати потрібний нам код, окремо від основного потоку, тобто асинхронно.
 Наприклад: завантаження данних із сервера асинхронно, допоможе не зупиняти виконання синхронного коду.
 */



const filmsList = document.querySelector(".filmList");


fetch("https://ajax.test-danit.com/api/swapi/films")
.then(data => data.json())

.then(data => {
   
   
   const sortedList = data.sort((a, b) => a.episodeId-b.episodeId)
    sortedList.forEach(element => {
    
    const film = document.createElement('li');
    const episodNumber = document.createElement('p');
    episodNumber.innerText = element.episodeId;
    const episodName = document.createElement('h2');
    episodName.innerText = element.name;
    const episodDescript = document.createElement('p')
    episodDescript.innerText = element.openingCrawl;

    const characters = document.createElement('ul');
   film.append(episodNumber, episodName, characters, episodDescript)

    filmsList.appendChild(film);

   element.characters.forEach(element =>{
    
       fetch(`${element}`)
       .then(data => data.json())
       .then(data => {       
            const character = document.createElement('li');
             character.innerText = data.name;
             characters.append(character)
        
       })
   }
   )
})})

